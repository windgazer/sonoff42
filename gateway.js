// System
const {
    SONOFF42_DEBUG,
} = process.env
const debug = SONOFF42_DEBUG === '1';

// Third Party
const tynt = require('tynt');

// Internal
const gateway = require('./src/ThingsboardGateway');
const server = require('./server');
const {
    get,
    replace,
} = require('./src/SonoffStore');

// Scope

// Connection needs refreshing now and again, which requires
// us to re-affirm device ownership. Ping will automatically trigger
// the right parts.
gateway.on('connect', server.ping);

gateway.on('rpc', (msg) => {
    // {"device":"eWeLink_1000b4cbf5.local","data":{"id":0,"method":"setValue","params":true}}
    const {
        device: deviceTarget,
        data: {
            params: value
        }
    } = msg;

    const device = get( { target: deviceTarget } );
    const method = value ? 'switchOn' : 'switchOff';
    if (device) {
        console.log(`Calling ${tynt.Cyan(method)} on ${tynt.Magenta(device.id)}`);
        device[method]()
            .then((newSonoffState) => replace(device, newSonoffState))
        ;
    }
});

server.on(function onMdnsResponse(device) {
    if (device.fresh) {
        gateway.connect(device);
        console.log('connecting device to thingsboard');
        device.fresh = false;
    }
    console.log(`updating device '${tynt.Magenta(device.id)}' to thingsboard`);
    gateway.update(device);
});
