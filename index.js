require('dotenv').config();
require('./server');
require('./gateway');
if (!process.argv.includes('--server')) {
    require('./interactive');
}
