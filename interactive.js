const ka = require('./src/KeepAlive');
const {
    values,
    replace,
} = require('./src/SonoffStore');
const {
    ping,
} = require('./server');

ka.setHandler('p', ping);
ka.setHandler('a', () => values().forEach(
    (sonoff) => sonoff.switchOn()
        .then((newSonoffState) => replace(sonoff, newSonoffState))
));
ka.setHandler('u', () => values().forEach(
    (sonoff) => sonoff.switchOff()
        .then((newSonoffState) => replace(sonoff, newSonoffState))
));
ka.setHandler('i', () => {
    values().forEach((sonoff) => {
        console.log(sonoff);
    })
})
ka.setHandler('d', () => values().forEach(
    (sonoff) => sonoff.deviceInfo()
        .then((newSonoffState) => replace(sonoff, newSonoffState))
));
