# Sonoff42

Provide an IoT gateway from Sonoff DiY devices to Thingsboard.io.

## Intro

All a Sonoff mini needs is a jumper correct wifi credentials. After that it's
easy to use simple HTTP posts and mDNS queries and responsed to know the state
of your Sonoff DiY.

Thingsboard.io provides a clear interface to makes rules and dashboards for IoT
devices. In recent releases they added an mqtt based gateway API, making it
easy to provision devices using your own middle-ware. This library provides a
middleware layer to translate Thingsboard mqtt commands to Sonoff DiY devices
and translates the mDNS responses back to Thingsboard.

## Features

- Auto-discovery of Sonoff DiY devices through mDNS
- Auto-provisioning of Thingsboard devices through a 'gateway device'
- Auto-updating of thingsboard for relevant properties

## Howto

- `npm install`
- copy `.env.example` to `.env`
- On Windows, set the `IP_CONFIG` specifically to the IP-address of the network
  interface you want to monitor.
  On Mac/Linux/Raspberry, set it to `0.0.0.0`.
- Setup device-gateway in Thingsboard and copy it's TOKEN
- Set Thingsboard IP in `.env`
- Set Thingsboard Token in `.env`
- `node index.js`

Any compatible Sonoff device is discovered on startup, or when such devices are
added to the network. Upon discovery, a device is registered in Thingsboard.
Using Thingsboard RPC, you can toggle switches on and off.

### Daemon

I've started using PM2, for the 'quickstart' see https://pm2.keymetrics.io/docs/usage/quick-start/

TLDR (assumes this app is cloned to ~/sonoff42):
- `sudo npm install pm2@latest -g`
- `cd ~/sonoff42`
- `pm2 start --name sonoff42  --watch --ignore-watch="node_modules" index.js -- --server`
- `pm2 startup systemd`
- `sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u johndoe --hp /home/johndoe`
- `sudo systemctl start pm2-johndoe`???

(To remove it `pm2 unstartup systemd`)
