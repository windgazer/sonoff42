// Third party
const fetch = require('node-fetch');
const txt = require('dns-txt')();

// Scope
const defaultFetchData = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    referrerPolicy: 'no-referrer',
}

const parsers = {
    'PTR': function parsePTR( answer ) {
        return {};
    },
    'TXT': function parseTXT( { data } ) {
        const txtData = data.map(
            (data) => txt.decode(data),
        ).reduce(
            (acc, data) => ({...acc, ...data}),
            {},
        );

        // JSON needs to be unpacked to be useful
        const parsedData = JSON.parse(txtData.data1);
        delete txtData.data1;

        return {
            ...txtData,
            ...parsedData,
        };
    },
    'SRV': function parseSRV( { data: { port, target } } ) {
        return {
            target,
            port,
        }
    },
    'A': function parseA( { data } ) {
        return {
            ip: data,
        }
    }
}

function fetchSonoff({ ip, port, id: deviceid }, uri = '/zeroconf/info', data = {}) {
    const url = `http://${ip}:${port}${uri}`;
    const body = {
        deviceid, 
        data,
    };
    return fetch(url, {
        ...defaultFetchData,
        body: JSON.stringify(body)
    }).then(
        (response) => response.json()
    );
}

function switchSonoff(sonoffData = {}, value = 'on'){
    const uri = '/zeroconf/switch'
    const data =  {
            "switch": value 
    }
    return fetchSonoff(sonoffData, uri, data)
        .then( (json) => ({
            ...sonoffData,
            ...json,
        }) )
    ;
}

function deviceInfo(sonoffData = {}) {
    return fetchSonoff(sonoffData, '/zeroconf/info', {})
        .then((json) => ({
            ...sonoffData,
            ...JSON.parse(json.data),
        }))
    ;
}

function sonoffParse(answers) {
    const sonoffData = answers.reduce( (acc, answer) => {
        const { type: answerType } = answer;
        const parser = parsers[ answerType ];
        return {
            ...acc,
            ...parser?parser(answer):{},
        }
    }, {});
    if (!sonoffData.id && sonoffData.target) {
        sonoffData.id = sonoffData.target.replace(/eWeLink_(.+).local/, '$1');
    }
    return sonoffData;
}

function sonoffBind(sonoffData) {
    return {
        ...sonoffData,
        switchOn: () => switchSonoff(sonoffData),
        switchOff: () => switchSonoff(sonoffData, 'off'),
        deviceInfo: () => deviceInfo(sonoffData),
    };
}

module.exports = {
    parse: function createSonoffObject(answers) {
        return sonoffBind(
            sonoffParse(answers),
        );
    }
}