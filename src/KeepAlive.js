// Third party
const readline = require('readline');

// Scope
const help = `To exit this process hit 'q' or 'ctrl-c'.`;

const handlers = {
    q: () => process.exit(),
    'ctrl-c': () => process.exit(),
    default: (key) => console.log(help, key),
};

function setHandler(key, handler) {
    handlers[key] = handler;
}

// Init
readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);
process.stdin.on('keypress', (str, key) => {
    const keyName = `${key.ctrl?'ctrl-':''}${key.name||str}`;
    const handler = handlers[keyName]||handlers.default;
    handler();
});
console.log(help);

module.exports = {
    setHandler
};
