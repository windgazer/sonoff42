let sonoffs = {};

function replace(sonoff, newSonoff) {
    const existingData = get(sonoff) || {};
    return put( {
        ...sonoff,
        ...existingData,
        ...newSonoff,
    } );
}

function get(sonoff = { target: '' }) {
    return sonoffs[sonoff.target];
}

function put(sonoff) {
    return sonoffs[sonoff.target] = sonoff;
}

module.exports = {
    replace,
    get,
    put,
    values: () => Object.values(sonoffs),
}