// System
const {
    THINGSBOARD_IP,
    THINGSBOARD_GATEWAY_TOKEN,
} = process.env

// Third Party
const tynt = require('tynt');
var mqtt = require('mqtt').connect(`mqtt://${THINGSBOARD_IP}`,{
    username: THINGSBOARD_GATEWAY_TOKEN
});

// Scope
mqtt.on('connect', function () {
    console.log(tynt.Cyan('connected'));
    mqtt.subscribe('v1/gateway/rpc');
    mqtt.subscribe('v1/gateway/attributes');
});

mqtt.on('message', function (topic, message) {
    console.log(`request.topic '${tynt.Magenta(topic)}'`);
    console.log(`request.body '${tynt.Magenta(message.toString())}'`);
});

function onRpc( cb ) {
    mqtt.on('message', function onMessageParser(topic, message) {
        if ( topic === 'v1/gateway/rpc') {
            const obj = JSON.parse(message.toString());
            cb(obj);
        }
    })
}

function on(onType, cb) {
    switch (onType) {
        case 'rpc':
            return onRpc(cb);
        case 'connect':
            return mqtt.on(onType, cb);
    }
    throw(`Method '${onType}' not implemented`);
}

function connectDeviceToThingsboard ( { target } ) {
    return mqtt.publish('v1/gateway/connect', JSON.stringify({
        "device": target
    }));
}

function updateDeviceToThingsboard ( device ) {
    const {
        startup,
        pulse,
        pulseWidth,
        id: deviceId,
    } = device;
    // Convert switch 'on'/'off' to boolean, easier to work with dashboards
    const switchValue = device.switch === 'on';
    mqtt.publish('v1/gateway/attributes', JSON.stringify({
        [device.target]: {
            switch: switchValue,
            startup,
            pulse,
            pulseWidth,
            deviceId
        }
    }));
}

module.exports = {
    connect: connectDeviceToThingsboard,
    update: updateDeviceToThingsboard,
    on,
}
