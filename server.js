// System
const {
    SONOFF42_DEBUG,
} = process.env
const debug = SONOFF42_DEBUG === '1';

// Third party
const tynt = require('tynt');
const mdns = require('multicast-dns')({
    // explicitly specify a network interface. defaults to all
    interface: process.env.IP_CONFIG||'0.0.0.0',
});

// Internal
const parser = require('./src/SonoffParser');
const {
    get,
    put,
    replace,
} = require('./src/SonoffStore');

// Scope
const responseHandlers = [];

mdns.on('query', ( { questions } ) => {
    const [ { name } ] = questions;
    if ( name.indexOf('ewe') > 0 ) {
        console.log(
            `Sending query to '${tynt.Magenta(name)}'`,
            debug?questions:questions.length
        );
    }
});

mdns.on('response', ({ answers }) => {
    const [ { name } ] = answers;

    if ( name.indexOf('ewelink') > 0 ) {

        const sonoffData = parser.parse(answers);
        const existingData = get(sonoffData) || {fresh: true};
        const device = put( {
            ...existingData,
            ...sonoffData,
        } )

        console.log(
            `Received response from '${tynt.Magenta(name)}'`,
            debug?answers:answers.length,
            debug?sonoffData:tynt.Magenta(sonoffData.id)
        );

        responseHandlers.forEach((cb) => cb(device));
    }
})

function ping() {
    return mdns.query('_ewelink._tcp.local', 'PTR');
}

module.exports = {
    ping,
    on: function onResponse(cb = function(){}) {
        responseHandlers.push(cb);
    }
}